// Contains instructions on HOW your API will perform its intended tasks
// All the operations it can do will be placed in this file

const Task = require("../models/task");

module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	})
};

// Controller function for ccreating a task
module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
	return newTask.save().then((task,error)=> {
		if (error){
			console.log(error);
			return false;
		}	else {
			return task;
		}
	})
};

// Controller funtion for deleting a task
/*
Business Logic
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, error) => {
		if (error){
			console.log(error)
			return false;
		} else {
			return "Deleted Task"
		}
	})
};


// GET specific by ID

module.exports.getSpecificTask = (taskId) => {
	return Task.findById(taskId).then((foundTask, error) => {
		if (error){
			console.log(error);
			return false;
		} else {
			return foundTask;
		}
	})
};



// Update by ID
// Stretch Goal
module.exports.updateSpecificTask = (taskId, requestBody) => {
	return Task.findByIdAndUpdate(taskId).then((foundTask,error) => {
			if (error){
			console.log(error);
			return false;
		}
			foundTask.status = requestBody.status;
			return foundTask.save().then((updatedTas, saveErr)=>{
				if (saveErr){
					console.log(saveErr);
					return false;
				} else {
					return "Task Updated! \n" + foundTask;
				}
			})
		

	})
};
