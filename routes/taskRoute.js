// Defined WHEN particular ontrollers will be used
// Contain all endpoints and response that we can get from all controllers


const express = require("express");
// Creates a router instance that function as a middleware and routing system

const router = express.Router();

const taskController = require("../controllers/taskController");

// Route to get all the tasks
// http://localhost:3001/tasks
router.get("/", (req,res)=> {
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});

// Route to create task
router.post("/", (req,res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Route to delete a task

router.delete("/:id", (req,res)=> {
	taskController.deleteTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


// Route to GET specific data
router.get("/:id", (req,res)=> {
	taskController.getSpecificTask(req.params.id).then(resultFromController => res.send(resultFromController));
})


// Route to Update specific data
router.put("/:id", (req,res)=> {
	taskController.updateSpecificTask(req.params.id, req.body).then(resultFromController => res.send(resultFromController));
})

module.exports = router;